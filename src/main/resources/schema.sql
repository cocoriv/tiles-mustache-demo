CREATE TABLE producto (
    id BIGINT identity primary key,
    nombre VARCHAR(255) NOT NULL
);

INSERT INTO producto
    (id,    nombre)
VALUES
    (0,     'Skate'),
    (1,     'Rodilleras');
