package com.somospnt.tiles.mustache.controller;


import com.google.common.base.Function;
import com.somospnt.tiles.mustache.repository.ProductoRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Invocable;
import javax.script.ScriptException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProductoController {

    @Autowired
    private ProductoRepository itemRepository;

    @RequestMapping(value = "/productos", method = RequestMethod.GET)
    public String home(Model model) throws ScriptException {
        model.addAttribute("productos", itemRepository.findAll());
        return "page.producto";
    }

}
