package com.somospnt.tiles.mustache.repository;

import com.somospnt.tiles.mustache.domain.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto, Long>{


}
